#!/usr/bin/env python3

# Name: contiguity.py
# Usage: see the output of python3 contiguity.py -h
# Description: Calculates sequence coverage and contiguity indexes based on a sorted BAM alignment
# Requires: functional installation of Python v3+ with `pysam`
# Author: Denis Jacob Machado
# Contact: dmachado[at]uncc.edu
# License: GNU's GPL v3
# The contiguity index was defined in Machado et al. (2018; http://doi.wiley.com/10.1002/ece3.3918)

#############
## MODULES ##
#############

import sys, argparse

try:
	import pysam # Can be installed with `pip3 install pysam`
except:
	sys.stderr.write("! ERROR: could not load pysam. This module can be installed using pip. See https://pysam.readthedocs.io/en/latest/index.html.\n")
	exit()

####################
## MAIN VARIABLES ##
####################

parser=argparse.ArgumentParser()
parser.add_argument("-i", "--input", help = "Sorted alignment in BAM format (must be indexed)", type = str, required = True)
parser.add_argument("-l", "--length", help = "Sequence length", type = int, required = True)
parser.add_argument("-s", "--seqname", help = "Sequence name", type = str, required = True)
args = parser.parse_args()

###############
## FUNCTIONS ##
###############

def read_bam(input):
	return pysam.AlignmentFile(input, "rb")

def get_reads(myfile, seqname, i, j):
	return set(["-".join(str(x).split()[0:2]) for x in myfile.fetch(seqname, i, j)])

def calc_cont(pre, cur, pos):
	contiguity = 0
	for i in cur:
		if i in pre and i in pos:
			contiguity += 1
	return contiguity

def iterate(myfile, seqname, i):
	pre = get_reads(myfile, seqname, i-2, i-1)
	cur = get_reads(myfile, seqname, i-1, i)
	pos = get_reads(myfile, seqname, i, i+1)
	coverage = len(cur)
	contiguity = calc_cont(pre, cur, pos)
	sys.stdout.write("{}, {}, {}, {}\n".format(seqname, i, coverage, contiguity))
	return

#############
## EXECUTE ##
#############

# Read BAM alignment
myfile = read_bam(args.input)

# CSV header
sys.stdout.write("# Sequence name, Position, Coverage, Contiguity\n")

# Fetching reads mapped to a region
for i in range(2, args.length - 1): # Calculates contiguity from the second to the penultimate database
	iterate(myfile, args.seqname, i)

exit()

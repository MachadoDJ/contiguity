# General description

A pysam-based script to calculate the coverage and contiguity of every position using a sorted and indexed BAM alignment. The contiguity index was defined in Machado et al. (2018; http://doi.wiley.com/10.1002/ece3.3918).

# Details

- Main script: contiguity.py
- Usage: see the output of `python3 contiguity.py -h`
- Description: Calculates sequence coverage and contiguity indexes based on a sorted BAM alignment
- Requires: functional installation of **Python v3+** with [`pysam`](https://pysam.readthedocs.io/en/latest/index.html)
- Author: Denis Jacob Machado
- Contact: dmachado[at]uncc.edu
- License: [*GNU's GPL v3*](https://www.gnu.org/licenses/gpl-3.0.en.html)

# The contiguity index

The contiguity index was defined in [Machado *et al.* (2018)](http://doi.wiley.com/10.1002/ece3.3918).

The contiguity index is similar to the coverage of a position, but it only counts the reads that do not start or end at that positon. Therefore, it serves to compute the number of sequence reads that supports the current nucleotide in relation to its neighbors.